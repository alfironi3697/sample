package com.kraveo.sample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.kraveo.sample.R;
import com.kraveo.sample.fragment.BlankFragment;

public class MainActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    public static final String PARAM_NAME = "param_name";
    public static final String PARAM_NUMBER = "param_number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.relative_xml);
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    public void onTextClick(View view){
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(PARAM_NAME, "my name");
        intent.putExtra(PARAM_NUMBER, 3);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed");
    }
}
