package com.kraveo.sample.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.kraveo.sample.R;
import com.kraveo.sample.fragment.BlankFragment;
import com.kraveo.sample.fragment.ChechBoxFragment;
import com.kraveo.sample.fragment.HomeFragment;
import com.kraveo.sample.fragment.NotificationFragment;
import com.kraveo.sample.fragment.ProductFragment;
import com.kraveo.sample.fragment.ProfileFragment;
import com.kraveo.sample.fragment.SendFragment;
import com.kraveo.sample.fragment.SettingFragment;
import com.kraveo.sample.fragment.ShareFragment;
import com.kraveo.sample.fragment.TextViewFragment;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = getClass().getSimpleName();

    public static final int PAGE_HOME = 0;
    public static final int PAGE_PROFILE = 1;
    public static final int PAGE_PRODUCT = 2;
    public static final int PAGE_SETTING = 3;
    public static final int PAGE_SHARE = 4;
    public static final int PAGE_SEND = 5;
    public static final int PAGE_NOTIF = 6;

    private Fragment currentFragment;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        selectPage(PAGE_HOME);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            selectPage(PAGE_SETTING);
            return true;
        } else if (id == R.id.action_notif) {
            selectPage(PAGE_NOTIF);
            return true;
        } else if (id == R.id.action_profile) {
            selectPage(PAGE_PROFILE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            selectPage(PAGE_HOME);
        } else if (id == R.id.nav_profile) {
            selectPage(PAGE_PROFILE);
        } else if (id == R.id.nav_product) {
            selectPage(PAGE_PRODUCT);
        } else if (id == R.id.nav_setting) {
            selectPage(PAGE_SETTING);
        } else if (id == R.id.nav_share) {
            selectPage(PAGE_SHARE);
        } else if (id == R.id.nav_send) {
            selectPage(PAGE_SEND);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void selectPage(int page) {
        switch (page) {
            case PAGE_HOME:
                if (!(currentFragment instanceof HomeFragment)) {
                    getSupportActionBar().setTitle("HOME");
                    currentFragment = HomeFragment.newInstance();
                }
                break;
            case PAGE_PRODUCT:
                if (!(currentFragment instanceof ProductFragment)) {
                    getSupportActionBar().setTitle("PRODUCT");
                    currentFragment = ProductFragment.newInstance();
                }
                break;
            case PAGE_PROFILE:
                if (!(currentFragment instanceof ProfileFragment)) {
                    getSupportActionBar().setTitle("PROFILE");
                    currentFragment = ProfileFragment.newInstance();
                }
                break;
            case PAGE_SETTING:
                if (!(currentFragment instanceof SettingFragment)) {
                    getSupportActionBar().setTitle("SETTING");
                    navigationView.getMenu().getItem(PAGE_SETTING).setChecked(true);
                    currentFragment = SettingFragment.newInstance();
                }
                break;
            case PAGE_SHARE:
                if (!(currentFragment instanceof ShareFragment)) {
                    getSupportActionBar().setTitle("SHARE");
                    currentFragment = ShareFragment.newInstance();
                }
                break;
            case PAGE_SEND:
                if (!(currentFragment instanceof SendFragment)) {
                    getSupportActionBar().setTitle("SEND");
                    currentFragment = SendFragment.newInstance();
                }
                break;
            case PAGE_NOTIF:
                if (!(currentFragment instanceof NotificationFragment)) {
                    getSupportActionBar().setTitle("Notification");
                    currentFragment = NotificationFragment.newInstance();
                }
                break;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, currentFragment)
                .commit();
    }
}
