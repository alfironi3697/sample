package com.kraveo.sample.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.kraveo.sample.R;
import com.kraveo.sample.activity.Main2Activity;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private Button btnSetting, btnProfile;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSetting = view.findViewById(R.id.button);
        btnProfile = view.findViewById(R.id.button2);
        btnSetting.setOnClickListener(this);
        btnProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSetting){
            ((Main2Activity)getActivity()).selectPage(Main2Activity.PAGE_SETTING);
        } else if (view == btnProfile){
            ((Main2Activity)getActivity()).selectPage(Main2Activity.PAGE_SETTING);
        }
    }
}
