package com.kraveo.sample.fragment;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.kraveo.sample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChechBoxFragment extends Fragment {

    public ChechBoxFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check_box, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CheckBox male = view.findViewById(R.id.male);
        CheckBox female = view.findViewById(R.id.female);

        RadioGroup radioGroup = view.findViewById(R.id.group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {
                    case R.id.male_radio:
                        Toast.makeText(getActivity(), "I'm male", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.female_radio:
                        Toast.makeText(getContext(), "I'm female", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Toast.makeText(getActivity(), "I'm male", Toast.LENGTH_SHORT).show();
                }
            }
        });

        female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Toast.makeText(getContext(), "I'm female", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
